/*
 * Copyright 2011 University of California, San Diego.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucsd.hep.rootrunnerutil;

import java.util.ArrayList;
import java.util.List;


/**
 * Class to produce ROOTRunner objects with a given set of parameters.
 * This class could become an interface in the future.
 * 
 * @author holzner
 */
public class ROOTRunnerFactory
{
  private boolean useCommandDisplayPanel;
  private String root_cmd_args;
  private String shell_initialization_commands;
  private PipeCommandRunnerWorkerFactory runnerFactory;

  /** do we need factories here to create a new set of listeners
   *  every time we create a ROOTRunner ? */
  private final List<PipeCommandRunnerListenerFactory> commandRunnerListenerFactories;
  
  /** commands to be executed after each ROOT startup */
  private String rootStartupCommands;
  private String rootCmd;

  //----------------------------------------------------------------------

  /** @param commandRunnerListenerFactories can be null (this is equivalent
   *  to passing an empty list)
   */
  public ROOTRunnerFactory(List<PipeCommandRunnerListenerFactory> commandRunnerListenerFactories, String root_cmd_args, String shell_initialization_commands, PipeCommandRunnerWorkerFactory runnerFactory)
  {
    if (commandRunnerListenerFactories == null)
      this.commandRunnerListenerFactories = new ArrayList<PipeCommandRunnerListenerFactory>();
    else
      this.commandRunnerListenerFactories = new ArrayList<PipeCommandRunnerListenerFactory>(commandRunnerListenerFactories);

    this.root_cmd_args = root_cmd_args;
    this.shell_initialization_commands = shell_initialization_commands;
    this.runnerFactory = runnerFactory;
  }

  //----------------------------------------------------------------------

  public ROOTRunnerFactory(boolean useCommandDisplayPanel)
  {
    this(null, null, null, null);
  }

  //----------------------------------------------------------------------

  public ROOTRunner make()
  {
    ArrayList<PipeCommandRunnerListener> listeners = new ArrayList<PipeCommandRunnerListener>();
    for (PipeCommandRunnerListenerFactory factory : this.commandRunnerListenerFactories)
      listeners.add(factory.make());
    
    ROOTRunner runner = new ROOTRunnerOld(listeners, root_cmd_args, shell_initialization_commands, runnerFactory);

    if (rootCmd != null)
      runner.setROOTCommand(rootCmd);

    if (rootStartupCommands != null)
      runner.writeLine(rootStartupCommands);
    return runner;
  }

  //----------------------------------------------------------------------

  public void setROOTStartupCommands(String rootStartupCommands)
  {
    this.rootStartupCommands = rootStartupCommands;
  }

  //----------------------------------------------------------------------

  public void setROOTCmd(String rootCmd)
  {
    this.rootCmd = rootCmd;
  }

  //----------------------------------------------------------------------

}
