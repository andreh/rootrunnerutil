/*
 * Copyright 2011 University of California, San Diego.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucsd.hep.rootrunnerutil;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 *
 * @author holzner
 */
public class AHUtils
{
  public final static String lineSeparator = System.getProperty("line.separator");

  //----------------------------------------------------------------------
  public static byte[] readFileToByteArray(String fname) throws FileNotFoundException, IOException
  {
    File fin = new File(fname);
    FileInputStream is = new FileInputStream(fin);
    long len = fin.length();

    byte retval[] = new byte[(int) len];
    is.read(retval);

    return retval;
  }
  //----------------------------------------------------------------------

  public static File writeTextToTemporaryFile(String text, String fname_prefix, String fname_suffix) throws IOException
  {
    File temp = File.createTempFile(fname_prefix, fname_suffix);

    BufferedWriter out = new BufferedWriter(new FileWriter(temp));
    out.write(text);
    out.close();

    return temp;
  }
  //----------------------------------------------------------------------
   /** note that this is not 100% safe because it first creates
   *  a temporary file which it then deletes and replaces by a directory
   *  of the same name.
   * @return
   * @throws IOException
   */
  public static File makeTemporaryDirectory() throws IOException
  {
    final File temp = File.createTempFile("tmp", Long.toString(System.nanoTime()));

    // delete the created file
    if (!temp.delete())
     throw new IOException("Could not delete temporary directory " + temp.getAbsolutePath());

    // produce a directory instead.
    if (!(temp.mkdir()))
      throw new IOException("Could not create temporary directory " + temp.getAbsolutePath());

    return temp;
  }

  //----------------------------------------------------------------------

  public static void writeStringToFile(File file, String data) throws FileNotFoundException
  {
    PrintWriter writer = new PrintWriter(new FileOutputStream(file));
    writer.print(data);
    writer.close();
  }
  //----------------------------------------------------------------------

  public static void writeStringToGzippedFile(File file, String data) throws IOException
  {
    PrintWriter writer = new PrintWriter(new GZIPOutputStream(new FileOutputStream(file)));
    writer.print(data);
    writer.close();
  }
  //----------------------------------------------------------------------

  public static String readFile(String fname) throws IOException
  {
    return readStream(new FileInputStream(fname));
  }
  //----------------------------------------------------------------------

  public static String readGzippedFile(String fname) throws IOException
  {
    return readStream(new GZIPInputStream(new FileInputStream(fname)));
  }
  //----------------------------------------------------------------------

  public static String readFile(File path) throws IOException 
  {
    return readStream(new FileInputStream(path));
  }

  //----------------------------------------------------------------------

  /** this also deals with certain (not all) unicode encodings */
  public static String readStream(InputStream is) throws IOException
  {

    // try to find out unicode encoding (see http://mindprod.com/jgloss/bom.html )
    // (at least some of them)
    byte buf[] = new byte[2];
    int res = is.read(buf,0,2);

    if (res == -1)
      return "";

    if (res == 1)
      return "" + ((char)buf[0]);

    String encoding = null;

    InputStreamReader raw_reader = null;

    String retval = "";

    if (buf[0] == (byte)0xfe && buf[1] == (byte)0xff)
      raw_reader = new InputStreamReader(is,"UTF-16BE");
    else if (buf[0] == (byte)0xff && buf[1] == (byte)0xfe)
      raw_reader = new InputStreamReader(is,"UTF-16LE");
    else
    {
      // assume it's default encoding...
      raw_reader = new InputStreamReader(is);
      retval += (char)buf[0];
      retval += (char)buf[1];
    }

    BufferedReader reader = new BufferedReader(raw_reader);

    final int bufsize = 102400;
    char charbuf[] = new char[bufsize];

    res = reader.read(charbuf,0,bufsize);
    while (res != -1)
    {
      retval += String.copyValueOf(charbuf, 0, res);
      res = reader.read(charbuf,0,bufsize);
    }


    reader.close();
    return retval;

  }

  //----------------------------------------------------------------------

  public static List<String> splitToLines(String text)
  {
    List<String> retval = new ArrayList<String>();
    BufferedReader reader = new BufferedReader(new StringReader(text));

    try
    {
      String line;

      while ((line = reader.readLine()) != null)
        retval.add(line);

    } catch (IOException e)
    {
      // this should not happen ?
      e.printStackTrace();
    }

    return retval;
  }

  //----------------------------------------------------------------------

  /** convenience method to create a list with a single element */
  public static <T> List<T> makeList(T item)
  {
    List<T> retval = new ArrayList<T>();
    
    retval.add(item);
    return retval;
  }

  //----------------------------------------------------------------------

  public static String linesToSingleString(Iterable lines)
  {
    String retval = "";

    for (Object line : lines)
    {
      if (line != null)
        retval += line.toString();
      retval += lineSeparator;
    }

    return retval;
  }

  //----------------------------------------------------------------------
  /** similar to pythons time.sleep(..) function */
  public static void sleepSeconds(double seconds)
  {
    try
    {
      Thread.sleep((long)Math.round(seconds * 1000));
    } catch (InterruptedException ex)
    {
      Logger.getLogger(AHUtils.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  //----------------------------------------------------------------------

  /** only reads stdout, not stderr */
  public static String readCommandOutput(String cmd) throws IOException
  {
    Process proc = Runtime.getRuntime().exec(cmd);
    
    InputStream procStdOut = proc.getInputStream();

    // InputStream procStdErr = proc.getErrorStream();

    // wait for the process to finish 
    waitForCommandCompletion(proc);

    // first read the stream (until EOF), then
     
    // System.err.println("ERR='" + AHUtils.readStream(procStdErr) +"'"); 
     
    String retval = AHUtils.readStream(procStdOut);

    // TODO: do we have to close this stream ?
    return retval;
  }

  //----------------------------------------------------------------------

  public static int waitForCommandCompletion(Process proc)
  {
   while (true)
    {
      try
      {
        // wait for the external process to complete, ignore
        // the exit status
        return proc.waitFor();
      } catch (InterruptedException ex)
      {
        Logger.getLogger(AHUtils.class.getName()).log(Level.SEVERE, null, ex);
      }
    } // while 
   
  }
}
