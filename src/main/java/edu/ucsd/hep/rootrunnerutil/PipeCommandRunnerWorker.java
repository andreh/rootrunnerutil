/*
 * Copyright 2011 University of California, San Diego.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucsd.hep.rootrunnerutil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * interface for implementations which can run commands using input / output pipes.
 * @author holzner
 */
public interface PipeCommandRunnerWorker
{
  /**
   * this must add a newline by using println(..)
   */
  public void writeLineImplementation(String line);

  public int waitForExit() throws InterruptedException, FileNotFoundException, IOException;

  /** must create a temporary directory (on the machine where ROOT is run)
   *  and returns its full path (on the target machine ?!).
   * @return
   */
  public File makeTemporaryDirectory();

  /** must read a file and return its content (e.g. on the local machine
      or on the remote machine if root is run remotely) */
  public byte[] getOutputFileContent(String fname);

  /** @return a stream where the output of the command is sent. */
  public InputStream getCommandStdoutStream();

  /** must return true if the process has exited already */
  public boolean hasExited();

}
