/*
 * Copyright 2011 University of California, San Diego.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucsd.hep.rootrunnerutil;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * creates and follows the ROOT process
 * @author holzner
 */
public abstract class ROOTRunner 
{
  //----------------------------------------------------------------------
  /** the pipe to the ROOT process */
  protected PipeCommandRunner root_pipe;
  protected String rootCmd = "root";
  /** shell commands to be executed before the root command */
  protected String initialization_commands;
  /** command line arguments to root executable */
  protected String root_cmd_args;
  protected PipeCommandRunnerWorkerFactory runnerFactory;
  
  /** ROOT's stdout: for waiting for completion */
  protected InputStream root_stdout_for_checkpointing;
  
  protected List<PipeCommandRunnerListener> commandRunnerListeners;
  protected boolean promptCleared = false;

  //----------------------------------------------------------------------
  
  public synchronized void writeLine(String line)
  {
    getROOTPipe().writeLine(line);
  }

  //----------------------------------------------------------------------
  public void writeLines(Collection<String> lines)
  {
    for (String line : lines)
    {
      writeLine(line);
    }
  }


  //----------------------------------------------------------------------
  /** waits for the current command pipes to complete. Does this
   *  by telling root to write a string to the output stream
   *  and waits until it can read it from ROOT's stdout
   */
  public abstract String waitForCompletion();

  //----------------------------------------------------------------------
  public void exitRoot()
  {
    this.writeLine(".q");
  }
  //----------------------------------------------------------------------

  protected PipeCommandRunner getROOTPipe()
  {
    try
    {
      if (root_pipe == null)
        startROOT();
    } catch (Exception ex)
    {
      System.err.println(ex);
      Logger.getLogger(ROOTRunnerOld.class.getName()).log(Level.SEVERE, null, ex);
    }
   
    return root_pipe;
  }

  //----------------------------------------------------------------------

  protected abstract void startROOT() throws Exception;

  //----------------------------------------------------------------------

  /** this allows e.g. to set the absolute path to the root command */
  public void setROOTCommand(String rootCmd)
  {
    if (this.root_pipe != null)
      throw new Error("root started already");
    this.rootCmd = rootCmd;
  }

  //----------------------------------------------------------------------

  protected Marker sendMarker()
  {
    Marker marker = new Marker();
    // System.out.println("SENDING MARKER " + marker_string);
    writeLine(marker.getCommand());
    return marker;
  }
  
  //----------------------------------------------------------------------

  protected void setPrompt(String prompt)
  {
    // TODO: should escape quotes here
    this.writeLine("((TRint*)gApplication)->SetPrompt(\"" + prompt + "\");");
    promptCleared = prompt.isEmpty();
  }
  
  //----------------------------------------------------------------------

  protected void clearPrompt()
  {
    if (!promptCleared)
    {
      // clear the command prompt
      setPrompt("");
      // do not echo our commands
      this.writeLine("((TRint*)gApplication)->SetEchoMode(0);");
    }
  }

  //----------------------------------------------------------------------
  /** runs a command inside root and returns its output.
   * 
   *  note that this is NOT intended for binary output but
   *  only for line-oriented output.
   */
  public abstract String getCommandOutput(String cmd) throws IOException;

  /** should run multiple commands in 'batch mode' */
  public abstract List<String> getMultipleCommandsOutputBatch(List<String> cmds) throws IOException;
  
  
  /** returns the contents of the file. This can e.g. be used to read files on a remote
   *  host where ROOT is executed */
  public abstract byte[] readFile(String fname) throws IOException;

  /** by default, will create a local temporary file but can also be
   *  used to create a temporary file remotely */
  public abstract String createTempFile(String prefix, String suffix) throws IOException;  
}
