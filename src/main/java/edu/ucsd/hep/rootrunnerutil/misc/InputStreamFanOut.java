/*
 * Copyright 2011 University of California, San Diego.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucsd.hep.rootrunnerutil.misc;

import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Watches an Stream and notifies several listeners if anything new
 * appears
 *
 * @author holzner
 */
public class InputStreamFanOut
{
  private final InputStream is;

  private final Thread watcher_thread;

  private final List<PipedOutputStream> client_streams = new ArrayList<PipedOutputStream>();

  //----------------------------------------------------------------------

  public InputStreamFanOut(InputStream is)
  {
    this.is = is;

    watcher_thread = new Thread()
    {

      @Override
      public void run()
      {
        try
        {
          check();
        } catch (IOException ex)
        {
          Logger.getLogger(InputStreamFanOut.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
      
    };

    // start periodic watching (TODO: should be moved out of constructor)
    watcher_thread.start();

  }

  //----------------------------------------------------------------------

  /** eternal loop */
  private void check() throws IOException
  {
    int char_buffer_size = 2048;
    final byte[] char_buffer = new byte[char_buffer_size];

    while (true)
    {
      // this should block until SOME data is available
      // (see http://docs.oracle.com/javase/6/docs/api/java/io/InputStream.html#read%28byte[],%20int,%20int%29 )
      
      int num_chars_read = this.is.read(char_buffer);
            
      if (this.client_streams.isEmpty())
        // nobody is interested in this InputStream currently
        continue;

      if (num_chars_read == -1)
      {// end of the input stream reached
        for (PipedOutputStream client_stream : this.client_streams)
        {
          client_stream.close();
        }
        return;
      }

      // distribute the values just read to all clients
      synchronized (this.client_streams)
      {
        // for (PipedOutputStream client_stream : this.client_streams)
        for (Iterator<PipedOutputStream> it = this.client_streams.iterator();
             it.hasNext();
             )
        {
          PipedOutputStream client_stream = it.next();
          
          // watch out for closed streams
          try
          {
            client_stream.write(char_buffer, 0, num_chars_read);
            client_stream.flush();
          }
          catch (IOException ex)
          {
            // TODO: we should find a better way of identifying
            // this particular case
            if (! ex.getMessage().equals("Pipe closed"))
              throw ex;
            else
              it.remove();
          }
        }
      }
    
    } // infinite loop
  }

  //----------------------------------------------------------------------

  /** register a new listener for this InputStream */
  public void addClient(PipedInputStream client_stream) throws IOException
  {
    synchronized (this.client_streams)
    {
      this.client_streams.add(new PipedOutputStream(client_stream));
    }
  }

  //----------------------------------------------------------------------

  /** this is most convenient method: this will create a new
   *  InputStream to which data read from the input stream will 
   *  be written as soon as it is available.
   * @return
   * @throws IOException 
   */
  public InputStream makeNewInputStream() throws IOException
  {
    PipedInputStream retval = new PipedInputStream();
    this.addClient(retval);
    return retval;
  }

  //----------------------------------------------------------------------

  /** convenience method to produce multiple clones of the given input stream */
  public static List<InputStream> makeClones(InputStream is, int numClones) throws IOException
  {
    List<InputStream> retval = new ArrayList<InputStream>();
    
    InputStreamFanOut fanout = new InputStreamFanOut(is);
    
    for (int i = 0; i < numClones; ++i)
      retval.add(fanout.makeNewInputStream());
    
    return retval;
  }
  
  //----------------------------------------------------------------------

}
