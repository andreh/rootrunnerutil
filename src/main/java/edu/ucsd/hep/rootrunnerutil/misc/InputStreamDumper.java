/*
 * Copyright 2012 University of California, San Diego.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucsd.hep.rootrunnerutil.misc;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class for debugging purposes: dumps an InputStream to a PrintStream
 * @author holzner
 */
public class InputStreamDumper extends Thread
{
  private final InputStream is;
  private final PrintStream output;
  private final String linePrefix;

  //----------------------------------------------------------------------

  public InputStreamDumper(PrintStream output, InputStream is, String linePrefix)
  {
    this.output = output;
    this.is = is;
    this.linePrefix = linePrefix;
  }

  //----------------------------------------------------------------------

  @Override
  public void run()
  {
    final int bufferSize = 1024;
    byte buffer[] = new byte[bufferSize];

    try
    {
      boolean isStartOfLine = true;

      // TODO: try to avoid using Thread.sleep(..) here
      while (true)
      {
        // this should be blocking until something is available
        // (or an error occurs)
        int res = is.read(buffer);
        if (res == -1)
          // EOF reached
          return;

        if (res > 0)
        {
          
          // use a Reader to read characters
          ByteArrayInputStream buf = new ByteArrayInputStream(buffer,0,res);
          InputStreamReader reader = new InputStreamReader(buf);
          
          int ch;
          while ((ch = reader.read()) != -1)
          {
            // TODO: make this platform independent
            char chch = (char)ch;
            
            if (isStartOfLine)
              output.print(this.linePrefix);
            
            output.print(chch);
            
            if (chch == '\n')
              isStartOfLine = true;
            else
              isStartOfLine = false;
          }
        }
        

      } // eternal loop
    } catch (IOException ex)
    {
      Logger.getLogger(InputStreamDumper.class.getName()).log(Level.SEVERE, null, ex);
    }

  }
  
  //----------------------------------------------------------------------

}
