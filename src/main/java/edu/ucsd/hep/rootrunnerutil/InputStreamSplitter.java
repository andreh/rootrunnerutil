/*
 * Copyright 2012 University of California, San Diego.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucsd.hep.rootrunnerutil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * takes an input stream, and reads it until a given string
 * is observed and allows to retrieve the data before that.
 * Similar to expect but without an external process.
 * 
 * Not a very efficient implementation but we don't need it
 * for high-throughput applications for the moment. Also
 * mixes streams and strings, so this is clearly intended
 * for reading string streams.
 * 
 * @author holzner
 */
public class InputStreamSplitter
{
  //----------------------------------------------------------------------

  private ByteArrayOutputStream recordingBuffer = new ByteArrayOutputStream();
  
  private boolean recordingEnabled = false;
  private final InputStream is;
  
  //----------------------------------------------------------------------
    
  public InputStreamSplitter(InputStream is)
  {
    this.is = is;
  }
   
  //----------------------------------------------------------------------

  /** this is for keeping the data read from the stream in memory */
  public void enableRecording(boolean enable)
  {
    this.recordingEnabled = enable;
  }
  
  //----------------------------------------------------------------------

  public void clearRecordings()
  {
    recordingBuffer.reset();
  }
  
  //----------------------------------------------------------------------

  /** @return the contents of the recording buffer */
  public String getRecordings()
  {
    return recordingBuffer.toString();
  }

  //----------------------------------------------------------------------

  /** reads up to the given string. Note that this blocks until
   *  the string 'marker' was seen or EOF is reached.
   * 
   * @return false if EOF was reached
  */
  
  public boolean readToString(String marker) throws IOException
  {
    int bufSize = marker.length();
    
    // TODO: what should we do with the characters in the buffer
    // when an exception is thrown ?
    
    BlockingQueue<Integer> readBuffer;
    
    if (this.recordingEnabled)
      readBuffer = new ArrayBlockingQueue<Integer>(bufSize);
    else
      readBuffer = null;
    
    String buf = "";
    
    // probably the worst implementation efficiency wise..
    // (should use an advanced string searching algorithm
    // with a state machine)
    for (;;)
    {
      int res = is.read();
      if (res == -1)
      {
        // EOF reached
        return false;
      }

      if (this.recordingEnabled)
      {
        // make space if necessary
        if (readBuffer.size() >= bufSize)
          this.recordingBuffer.write(readBuffer.poll());
        try
        {
          readBuffer.put(res);
        } catch (InterruptedException ex)
        {
          Logger.getLogger(InputStreamSplitter.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
      
      // add it to the string buffer
      // really bad efficiency here
      buf = buf + (char)res;
      if (buf.length() > bufSize)
        buf = buf.substring(1, bufSize + 1);
      
      if (buf.equals(marker))
        return true;
      
    } // quasi eternal loop
    
  }
  
  //----------------------------------------------------------------------

  
}
