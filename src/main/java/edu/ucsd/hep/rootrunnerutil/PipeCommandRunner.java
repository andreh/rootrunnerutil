/*
 * Copyright 2011 University of California, San Diego.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucsd.hep.rootrunnerutil;

import edu.ucsd.hep.rootrunnerutil.misc.InputStreamFanOut;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Helper class for using a PipeCommandRunnerWorker (subclasses of which contain
 * the actual implementation details of how to run commands etc.)
 * 
 * @author holzner
 */
public class PipeCommandRunner
{
  private final List<PipeCommandRunnerListener> listeners = new ArrayList<PipeCommandRunnerListener>();
  
  private final PipeCommandRunnerWorker worker;
  private final InputStreamFanOut stdoutFanout;
  private InputStream stdout;

  private final BufferedReader stdoutForListeners;
  
  //----------------------------------------------------------------------

  public PipeCommandRunner(PipeCommandRunnerWorker worker) throws IOException
  {
    this.worker = worker;
    
    InputStream origStdout = worker.getCommandStdoutStream();
    
    stdoutFanout = new InputStreamFanOut(origStdout);
    
    stdoutForListeners = new BufferedReader(new InputStreamReader(stdoutFanout.makeNewInputStream()));
    
    
    // add a listener thread which notifies the PipeCommandRunnerListener objects
    // whenever we get new output from the external command we run
    Thread thread = new Thread()
    {

      @Override
      public void run()
      {
        try
        {
          while (true)
          {
            String line = stdoutForListeners.readLine();
            if (line == null)
              // EOF reached
              break;

            lineReceivedFromCommand(line);

          }
        } catch (IOException ex)
        {
          Logger.getLogger(PipeCommandRunner.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
      
    };
    
    // TODO: should not start the thread in the constructor
    thread.start();
  }
  
  //----------------------------------------------------------------------

  public File makeTemporaryDirectory()
  {
    return worker.makeTemporaryDirectory();
  }

  //----------------------------------------------------------------------

  public byte[] getOutputFileContent(String fname)
  {
    return worker.getOutputFileContent(fname);
  }
  
  //----------------------------------------------------------------------
  
  
  /** this is called whenever a line from the command run
   *  in the pipe is received
   */
  private void lineReceivedFromCommand(String line)
  {
    // System.out.println("GOT LINE " + line);
    // notify the listeners
    synchronized (this.listeners)
    {
      for (PipeCommandRunnerListener listener : this.listeners)
      {
        listener.appendCommandOutput(line);
      }
    }
    
  }

  //----------------------------------------------------------------------

  public void addListener(PipeCommandRunnerListener listener)
  {
    synchronized (this.listeners)
    {
      this.listeners.add(listener);
    }
  }

  //----------------------------------------------------------------------

  public void writeLine(String line)
  {
    worker.writeLineImplementation(line);

    // notify the listeners
    synchronized (this.listeners)
    {
      for (PipeCommandRunnerListener listener : this.listeners)
      {
        listener.appendCommandInputLine(line);
      }
    }
  }

  //----------------------------------------------------------------------

  synchronized InputStream getCommandStdoutStream() throws IOException
  {
    if (this.stdout == null)
      this.stdout = this.stdoutFanout.makeNewInputStream();
    
    return this.stdout;
  }

  //----------------------------------------------------------------------

  public boolean hasExited()
  {
    return worker.hasExited();
  }

}
