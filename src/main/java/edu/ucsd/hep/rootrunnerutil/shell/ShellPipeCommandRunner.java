/*
 * Copyright 2011 University of California, San Diego.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucsd.hep.rootrunnerutil.shell;

import edu.ucsd.hep.rootrunnerutil.AHUtils;
import edu.ucsd.hep.rootrunnerutil.PipeCommandRunnerWorker;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author holzner
 */
public class ShellPipeCommandRunner implements PipeCommandRunnerWorker
{
  /** the process object */
  private Process proc;

  /** to write data to the process's standard input */
  private PrintWriter proc_stdin_writer;
  private final InputStream procStdOut;
  private final InputStream procStdErr;

  //----------------------------------------------------------------------

  public ShellPipeCommandRunner(String cmd) throws IOException
  {
//    // create a temporary file with the command in it
//    // Delete temp file when program exits.
//    File cmd_script = AHUtils.writeTextToTemporaryFile(
//      "#!/bin/sh" + "\n" +
//      cmd + " 2>&1\n",
//      "cmd",
//      ".sh"
//      ); // merge stdout and stderr for the moment
//
//    cmd_script.setExecutable(true);
//    cmd_script.deleteOnExit();
//
//    // System.out.println("XXX " + slave_cmd_script.getAbsolutePath());
//    proc = Runtime.getRuntime().exec(
//      new String[]
//      {
//        "/bin/sh", "-c", cmd_script.getAbsolutePath()
//      });

    
    // List<String> args = new ArrayList<String>();
    
    if (false)
    {
      // TODO: note that this currently belongs to the calling project  
      // TODO: do we need it actually other than for debugging ?
      // args.add("/usr/bin/python");
      // args.add("private-testdata/process-wrapper.py");
    }

    // Note that if we start this e.g. from Netbeans directly
    // we might not have the PATH properly set up...
    // args.add("root");
    // args.add("-b");
    
    // ProcessBuilder pb = new ProcessBuilder(args);
    // Map<String, String> env = pb.environment();
    // proc = pb.start();
    
    proc = Runtime.getRuntime().exec(cmd);
    
    // System.out.println("working dir=" + pb.directory() + " " + System.getProperty("user.dir"));
    // System.out.println("STARTED command '" + Joiner.on(" ").join(pb.command()) + "'");
    
    OutputStream proc_stdin = proc.getOutputStream();
    proc_stdin_writer = new PrintWriter(proc_stdin);
    //--------------------

    procStdOut = proc.getInputStream();
    procStdErr = proc.getErrorStream();

  }
  //----------------------------------------------------------------------

  public int waitForExit() throws InterruptedException, FileNotFoundException, IOException
  {
    proc.waitFor();
    return proc.exitValue();

  }
  //----------------------------------------------------------------------
  /** adds a newline */
  public void writeLineImplementation(String line)
  {
    proc_stdin_writer.println(line);
    proc_stdin_writer.flush();
  }
  //----------------------------------------------------------------------

  public File makeTemporaryDirectory()
  {
    try
    {
      return AHUtils.makeTemporaryDirectory();
    } catch (IOException ex)
    {
      Logger.getLogger(ShellPipeCommandRunner.class.getName()).log(Level.SEVERE, null, ex);
      return null;
    }
  }

  //----------------------------------------------------------------------

  public byte[] getOutputFileContent(String fname)
  {
    try
    {
      return AHUtils.readFileToByteArray(fname);
    } catch (FileNotFoundException ex)
    {
      Logger.getLogger(ShellPipeCommandRunner.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IOException ex)
    {
      Logger.getLogger(ShellPipeCommandRunner.class.getName()).log(Level.SEVERE, null, ex);
    }

    return null;
  }

  //----------------------------------------------------------------------

  public InputStream getStdErrStream()
  {
    return procStdErr;
  }

  //----------------------------------------------------------------------

  public InputStream getStdOutStream()
  {
    return procStdOut;
  }

  //----------------------------------------------------------------------

  public InputStream getCommandStdoutStream()
  {
    // throw new UnsupportedOperationException("Not supported yet.");
    return getStdOutStream();
  }
  
  //----------------------------------------------------------------------

  public boolean hasExited()
  {
    try
    {
    proc.exitValue();
    return true;
    }
    catch (IllegalThreadStateException ex)
    {
      // still running
      return false;
    }
  }

  //----------------------------------------------------------------------
}
